#include "../h/memory_allocator.hpp"

MemoryAllocator::MemoryAllocator()
{
    /*

      +---------------+ HEAP_START_ADDR   <-------------------------- m_free_mem_head
      |      NULL     |  next
      +---------------+
      |      NULL     |  prev
      +---------------+
      |               |  size = HEAP_END_ADDR - HEAP_START_ADDR
      +---------------+
      |               |
      |               |
      |               |
      |               |
      |               |
      |       F       |
      |       R       |
      |       E       |
      |       E       |
      |               |
      |               |
      |               |
      |               |
      |               |
      +---------------+ HEAP_END_ADDR - 1

  */

    m_free_mem_head = (MemNode *) HEAP_START_ADDR;

    m_free_mem_head->size = (uint64) ((char *) HEAP_END_ADDR -
                                      (char *) HEAP_START_ADDR - sizeof(MemNode));
    // size of free segment in bytes that can be allocated
    m_free_mem_head->next = nullptr;
    m_free_mem_head->prev = nullptr;

    m_alloc_mem_head = nullptr;
}

MemoryAllocator &MemoryAllocator::get_instance()
{
    static MemoryAllocator instance;
    return instance;
}

void MemoryAllocator::insert_node(MemNode **head, MemNode *node)
{
    // Case 1: No head

    if (!(*head))
    {
        (*head) = node;
        node->next = nullptr;
        node->prev = nullptr;
        return;
    }

    MemNode *curr = (*head), *prev = nullptr;

    while (curr && curr < node)
    {
        prev = curr;
        curr = curr->next;
    }

    if (!prev)
    {
        // Case 2: Insert at the beginning

        node->next = curr;
        node->prev = nullptr;
        curr->prev = node;
        (*head) = node;
    }
    else
    {
        if (!curr)
        {
            // Case 3: Insert at the end

            prev->next = node;
            node->prev = prev;
            node->next = nullptr;
        }
        else
        {
            // Case 4: Insert in the middle

            prev->next = node;
            node->prev = prev;
            curr->prev = node;
            node->next = curr;
        }
    }
}

void MemoryAllocator::remove_node(MemNode **head, MemNode *node)
{
    if (!(*head) || !node)// no elements in list or node is null
    {
        return;
    }

    if (node == (*head))// node that will be removed is at the beginning
    {
        (*head) = node->next;
    }

    if (node->next)// node that will be removed has next element
    {
        node->next->prev = node->prev;
    }

    if (node->prev)// node that will be removed has previous element
    {
        node->prev->next = node->next;
    }
}

void MemoryAllocator::try_to_join(MemNode *node)
{
    /*

      +---------------+ --+ node --------------------------------+
      |               |   |                                      |
      |     FREE      |   |---> node->size                       |
      |               |   |                                      |
      +---------------+ --+ node + node->size == node->next      |----> node->size + node->next->size
      |               |   |                                      |
      |     FREE      |   |---> node->next->size                 |
      |               |   |                                      |
      +---------------+ --+ node->next + node->next->size -------+
      |               |
      |       A       |
      |       L       |
      |       L       |
      |       O       |
      |       C       |
      |               |
      +---------------+ --+ node->prev -------------------------------+
      |               |   |                                           |
      |     FREE      |   |---> node->prev->size                      |
      |               |   |                                           |
      +---------------+ --+ node == node->prev + node->prev->size     |----> node->prev->size + node->size
      |               |   |                                           |
      |     FREE      |   |---> node->size                            |
      |               |   |                                           |
      +---------------+ --+ node + node->size ------------------------+
      |               |
      |       A       |
      |       L       |
      |       L       |
      |       O       |
      |       C       |
      |               |
      +---------------+
      |               |
      |     FREE      |
      |               |
      +---------------+

  */

    if (node->next)
    {
        if ((char *) node + sizeof(MemNode) + node->size == (char *) node->next)
        {
            node->size += (node->next->size + sizeof(MemNode));
            node->next = node->next->next;
            if (node->next)
            {
                node->next->prev = node;
            }
        }
    }
    if (node->prev)
    {
        if ((char *) node->prev + sizeof(MemNode) + node->prev->size ==
            (char *) node)
        {
            node->prev->size += (node->size + sizeof(MemNode));
            node->prev->next = node->next;
            if (node->prev->next)
            {
                node->prev->next->prev = node->prev;
            }
        }
    }
}

void *MemoryAllocator::malloc(size_t size)
{
    /*

      +---------------+
      |               |
      |     FREE      |
      |               |
      +---------------+
      |               |
      |       A       |
      |       L       |
      |       L       |
      |       O       |
      |       C       |
      |               |
      +---------------+ --+ first_fit ----------------------------------------+
      | MemNode-alloc |   |                                                   |
      +---------------+   | <--------------- addr                             |
      |               |   |---> size                                          |
      |   ALLOCATED   |   |                                                   |
      |_______________| --+ first_fit + size                                  |----> first_fit->size
      | MemNode-free  | --+ newFragment                                       |
      +---------------+   |                                                   |
      |               |   |                                                   |
      |     FREE      |   |---> newFragment->size = first_fit->size - size    |
      |               |   |                                                   |
      +---------------+ --+ --------------------------------------------------+
      |               |
      |       A       |
      |       L       |
      |       L       |
      |       O       |
      |       C       |
      |       A       |
      |       T       |
      |       E       |
      |       D       |
      |               |
      +---------------+
      |               |
      |     FREE      |
      |               |
      +---------------+

  */

    if (!size)
        return nullptr;

    if (size > (size_t) ((char *) HEAP_END_ADDR - (char *) HEAP_START_ADDR))
        return nullptr;

    if (!m_free_mem_head)
        return nullptr;// no free segment

    MemNode *first_fit = nullptr;

    for (MemNode *curr = m_free_mem_head; curr; curr = curr->next)
    {
        if (curr->size >= size)
        {
            first_fit = curr;
            break;
        }
    }

    if (!first_fit)
        return nullptr;

    char *addr = (char *) first_fit +
                 sizeof(MemNode);// beginning of newly allocated segment

    remove_node(&m_free_mem_head,
                first_fit);// remove the first_fit node from the free list

    // if first_fit node has remaining free space after allocation, insert it into
    // the free list
    if (first_fit->size - size > sizeof(MemNode))
    {
        auto *new_fragment =
                (MemNode *) ((char *) first_fit + size + sizeof(MemNode));
        new_fragment->size = first_fit->size - sizeof(MemNode) - size;
        insert_node(&m_free_mem_head, new_fragment);
    }
    else
    {
        size = first_fit->size;
    }

    ((MemNode *) first_fit)->size = size;
    insert_node(&m_alloc_mem_head,
                first_fit);// insert allocated node into allocated nodes list

    return (void *) addr;
}

int MemoryAllocator::free(void *addr)
{

    /*
      +---------------+
      |               |
      |     FREE      |
      |               |
      +---------------+
      |               |
      |       A       |
      |       L       |
      |       L       |
      |       O       |
      |       C       |
      |               |
      +---------------+ <--------------- addr - sizeof(MemNode)
      | MemNode-alloc |
      +---------------+ <--------------- addr
      |               |
      |       A       |
      |       L       |
      |       L       |
      |       O       |
      |       C       |
      |               |
      +---------------+
      |               |
      |       A       |
      |       L       |
      |       L       |
      |       O       |
      |       C       |
      |               |
      +---------------+
      |               |
      |     FREE      |
      |               |
      +---------------+

  */

    addr = (char *) addr - sizeof(MemNode);
    for (MemNode *curr = m_alloc_mem_head; curr; curr = curr->next)
    {
        if (curr == addr)
        {
            remove_node(&m_alloc_mem_head, (MemNode *) addr);
            insert_node(&m_free_mem_head, (MemNode *) addr);
            try_to_join((MemNode *) addr);
            return 0;
        }
    }
    return -1;
}
