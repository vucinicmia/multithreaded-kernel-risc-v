#include "../h/scheduler.hpp"

SinglyLinkedList<_thread> Scheduler::m_ready_thread_queue;

_thread *Scheduler::get() { return m_ready_thread_queue.pop_front(); }

void Scheduler::put(_thread *tcb) { m_ready_thread_queue.push_back(tcb); }