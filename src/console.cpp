#include "../h/console.hpp"

Console &Console::get_instance()
{
    static Console instance;
    return instance;
}

void Console::put_c(char c) { m_buffer_out.insert(c); }

char Console::get_c() { return m_buffer_in.remove(); }

void Console::interrupt_handler()
{
    while ((*((char *) CONSOLE_STATUS) & CONSOLE_RX_STATUS_BIT) &&
           m_buffer_in.size() < m_buffer_in.size())
    {
        m_buffer_in.insert(*((char *) CONSOLE_RX_DATA));
    }
}

void Console::output_polling()
{
    while (*((char *) CONSOLE_STATUS) & CONSOLE_TX_STATUS_BIT)
    {
        *((char *) CONSOLE_TX_DATA) = m_buffer_out.remove();
    }
}

int Console::output_buffer_size() { return m_buffer_out.size(); }
