#include "../h/riscv.hpp"
#include "../h/thread.hpp"

void userMain();

void main()
{

    Riscv::w_stvec((uint64) &Riscv::supervisorTrap);

    _thread *main = _thread::create_thread(nullptr, nullptr, nullptr);
    _thread::running = main;

    auto *buffer_output_thread = _thread::create_thread(
            [](void *arg) {
                while (true)
                {
                    sys_call_wrapper({OUTPUT_POLLING});
                }
            },
            nullptr,
            (uint64 *) MemoryAllocator::get_instance().malloc(DEFAULT_STACK_SIZE *
                                                              sizeof(uint64)));

    buffer_output_thread->start_thread();

    volatile int user_main_finished = 0;

    _thread *user_main = _thread::create_thread(
            [](void *arg) {
                userMain();

                while (get_output_buffer_size() > 0)
                {
                    thread_dispatch();
                }

                *((int *) arg) = 1;
            },
            (void *) &user_main_finished,
            (uint64 *) MemoryAllocator::get_instance().malloc(DEFAULT_STACK_SIZE *
                                                              sizeof(uint64)));

    user_main->set_user_thread(true);

    user_main->start_thread();

    Riscv::ms_sstatus(Riscv::SSTATUS_SIE);

    while (!user_main_finished)
    {
        thread_dispatch();
    }

    Riscv::mc_sstatus(Riscv::SSTATUS_SIE);

    delete user_main;
    delete buffer_output_thread;
    _thread::delete_idle();
    delete main;
}
