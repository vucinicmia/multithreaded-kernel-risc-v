#include "../h/syscall_cpp.hpp"

struct PeriodicThreadParams {
    time_t period;
    PeriodicThread *this_pointer;

    PeriodicThreadParams(time_t period, PeriodicThread *thisPointer)
        : period(period), this_pointer(thisPointer) {}
};

PeriodicThread::PeriodicThread(time_t period)
    : Thread(
              [](void *_params) {
                  auto params = (PeriodicThreadParams *) _params;
                  time_t period = params->period;
                  PeriodicThread *this_pointer = params->this_pointer;

                  while (true)
                  {
                      this_pointer->periodicActivation();
                      time_sleep(period);
                  }
              },
              (void *) (new PeriodicThreadParams(period, this))

      )
{
}
