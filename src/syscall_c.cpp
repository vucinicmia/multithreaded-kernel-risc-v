#include "../h/syscall_c.h"
#include "../h/utils_sys_calls.hpp"
#include "../lib/mem.h"

uint64 sys_call_wrapper(syscall_params _params)
{
    LOAD_SYSCALL_PARAMETERS_IN_REGISTERS();

    ECALL;

    uint64 return_value;

    READ_RETURN_VALUE(return_value);

    return return_value;
}

void *mem_alloc(size_t size)
{
    uint64 sizeInBlocks = (size + MEM_BLOCK_SIZE - 1) / MEM_BLOCK_SIZE;
    return (void *) sys_call_wrapper({MEM_ALLOC, sizeInBlocks});
}

int mem_free(void *address)
{
    return (int) sys_call_wrapper({MEM_FREE, (uint64) address});
}

int thread_create(thread_t *handle, void (*start_routine)(void *), void *arg)
{
    return (int) sys_call_wrapper(
            {THREAD_CREATE, (uint64) handle, (uint64) start_routine, (uint64) arg,
             (uint64) __mem_alloc(DEFAULT_STACK_SIZE * sizeof(uint64))});
}

int thread_exit() { return (int) sys_call_wrapper({THREAD_EXIT}); }
void thread_dispatch() { sys_call_wrapper({THREAD_DISPATCH}); }

int cpp_api_thread_create(thread_t *handle, void (*start_routine)(void *),
                          void *arg)
{
    return (int) sys_call_wrapper(
            {CPP_API_THREAD_CREATE, (uint64) handle, (uint64) start_routine,
             (uint64) arg, (uint64) __mem_alloc(DEFAULT_STACK_SIZE * sizeof(uint64))});
}

int cpp_api_thread_start(thread_t handle)
{
    return (int) sys_call_wrapper({CPP_API_THREAD_START, (uint64) handle});
}

int is_finished_thread(thread_t handle)
{
    return (int) sys_call_wrapper({IS_THREAD_FINISHED, (uint64) handle});
}

int sem_open(sem_t *handle, unsigned init)
{
    return (int) sys_call_wrapper({SEM_OPEN, (uint64) handle, (uint64) init});
}

int sem_close(sem_t handle)
{
    return (int) sys_call_wrapper({SEM_CLOSE, (uint64) handle});
}

int sem_wait(sem_t id) { return (int) sys_call_wrapper({SEM_WAIT, (uint64) id}); }

int sem_signal(sem_t id)
{
    return (int) sys_call_wrapper({SEM_SIGNAL, (uint64) id});
}

int time_sleep(time_t time)
{
    return (int) sys_call_wrapper({TIME_SLEEP, (uint64) time});
}

char getc() { return (char) sys_call_wrapper({GET_C}); }

void putc(char c) { sys_call_wrapper({PUT_C, (uint64) c}); }

int get_output_buffer_size()
{
    return (int) sys_call_wrapper({OUTPUT_BUFFER_SIZE});
}