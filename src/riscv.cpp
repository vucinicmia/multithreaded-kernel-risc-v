#include "../h/riscv.hpp"
#include "../h/console.hpp"
#include "../h/sleep_list.hpp"

syscall_params Riscv::params;

extern uint64 (*sys_calls[SYS_CALL_NUMBER])(syscall_params);

void Riscv::popSppSpie()
{
    __asm__ volatile("csrw sepc, ra");
    __asm__ volatile("sret");
}

void Riscv::handleSupervisorTrap()
{

    const uint64 scause = r_scause();
    const uint64 sstatus = r_sstatus();

    if (scause == 0x0000000000000008UL ||
        scause == 0x0000000000000009UL)// system call
    {
        uint64 sepc = r_sepc() + 4;

        uint64 t0, t1;

        SAVE_REGISTER("t0", t0);
        SAVE_REGISTER("t1", t1);

        READ_SYSCALL_PARAMETERS_FROM_REGISTERS(params);

        RESTORE_REGISTER("t0", t0);
        RESTORE_REGISTER("t1", t1);

        uint64 systemCallNumber = params.p0;

        uint64 returnValue = sys_calls[systemCallNumber](params);

        RETURN_VALUE((uint64) returnValue);

        w_sepc(sepc);
    }
    else if (scause == 0x8000000000000001UL)// timer
    {
        SleepList::timer_interrupt_handler();
        _thread::time_slice_counter++;
        if (_thread::time_slice_counter >= _thread::running->get_time_slice())
        {
            uint64 sepc = r_sepc();
            _thread::dispatch();
            w_sepc(sepc);
        }
        mc_sip(SIP_SSIP);
    }
    else if (scause == 0x8000000000000009UL)
    {
        int irq = plic_claim();

        if (irq == CONSOLE_IRQ)
        {

            Console::get_instance().interrupt_handler();
        }

        plic_complete(irq);
    }
    else
    {
        // unexpected trap cause
        __putc('0' + scause);
    }

    w_sstatus(sstatus);
}