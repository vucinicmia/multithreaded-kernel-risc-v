#include "../h/syscall_cpp.hpp"
#include "../h/thread.hpp"

Thread::Thread(void (*body)(void *), void *arg)
{
    cpp_api_thread_create(&myHandle, body, arg);
}

Thread::Thread()
{
    cpp_api_thread_create(
            &myHandle,
            [](void *thread) {
                if (thread)
                    ((Thread *) thread)->run();
            },
            this);
}

Thread::~Thread() noexcept
{
    while (!is_finished_thread(myHandle))
        ;
    delete (thread_t) myHandle;
}

int Thread::start() { return cpp_api_thread_start(myHandle); }

void Thread::dispatch() { thread_dispatch(); }

int Thread::sleep(time_t time) { return time_sleep(time); }
