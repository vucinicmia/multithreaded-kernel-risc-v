#include "../h/sleep_list.hpp"

int SleepList::m_num_of_elem = 0;
SleepList::Node *SleepList::m_head = nullptr;

void SleepList::insert_element(_thread *_sleeping_thread, int _time_sleep)
{
    _sleeping_thread->set_sleeping(true);

    Node *curr = m_head, *prev = nullptr, *new_node;

    while (curr && curr->time_sleep < _time_sleep)
    {
        prev = curr;
        curr = curr->next;
    }

    new_node = new Node(_sleeping_thread, _time_sleep, curr);

    if (prev)
    {
        prev->next = new_node;
    }
    else
    {
        m_head = new_node;
    }

    m_num_of_elem++;
}

void SleepList::timer_interrupt_handler()
{
    if (!m_head)
        return;
    Node *curr = m_head;

    while (curr)
    {
        curr->time_sleep--;
        curr = curr->next;
    }

    while (m_head && m_head->time_sleep <= 0)
    {
        curr = m_head;
        curr->thread->set_sleeping(false);
        Scheduler::put(curr->thread);
        m_head = m_head->next;
        m_num_of_elem--;
        delete curr;
    }
}

int SleepList::sleep(time_t _time_sleep)
{
    insert_element(_thread::running, (int) _time_sleep);
    _thread::dispatch();
    return 0;
}
