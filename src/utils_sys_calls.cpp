#include "../h/utils_sys_calls.hpp"
#include "../h/console.hpp"
#include "../h/semaphore.hpp"
#include "../h/sleep_list.hpp"
#include "../h/syscall_c.h"
#include "../h/thread.hpp"

static uint64 mem_alloc(syscall_params params)
{
    uint64 size = params.p1;
    return (uint64) MemoryAllocator::get_instance().malloc(size * MEM_BLOCK_SIZE);
}

static uint64 mem_free(syscall_params params)
{
    uint64 addr = params.p1;
    return (uint64) MemoryAllocator::get_instance().free((void *) addr);
}

static uint64 thread_create(syscall_params params)
{
    uint64 handle = params.p1, start_routine = params.p2, arg = params.p3,
           stack = params.p4;
    int return_value;
    if (!handle)
    {
        return_value = -1;
    }
    else
    {
        *((thread_t *) handle) = _thread::create_and_start_thread(
                (void (*)(void *)) start_routine, (void *) arg, (uint64 *) stack);
        (*((thread_t *) handle))->set_user_thread(true);
        return_value = 0;
    }
    return (uint64) return_value;
}

static uint64 thread_exit(syscall_params params)
{
    return (uint64) _thread::exit();
}

static uint64 thread_dispatch(syscall_params params)
{
    _thread::dispatch();
    return 0;
}

static uint64 thread_yield(syscall_params params)
{
    _thread::dispatch();
    return 0;
}

static uint64 cpp_api_thread_create(syscall_params params)
{
    uint64 handle = params.p1, start_routine = params.p2, arg = params.p3,
           stack = params.p4;
    int return_value;
    if (!handle)
    {
        return_value = -1;
    }
    else
    {
        *((thread_t *) handle) = _thread::create_thread(
                (void (*)(void *)) start_routine, (void *) arg, (uint64 *) stack);
        (*((thread_t *) handle))->set_user_thread(true);
        return_value = 0;
    }
    return (uint64) return_value;
}

static uint64 cpp_api_thread_start(syscall_params params)
{
    uint64 handle = params.p1;
    int return_value;
    if (!handle)
    {
        return_value = -1;
    }
    else
    {
        ((thread_t) handle)->start_thread();
        return_value = 0;
    }
    return (uint64) return_value;
}

static uint64 is_finished_thread(syscall_params params)
{
    uint64 handle = params.p1;
    int return_value;
    if (!handle)
    {
        return_value = -1;
    }
    else
    {
        auto t = (thread_t) handle;
        if (!t->is_finished())
        {
            return_value = 0;
        }
        else
        {
            return_value = 1;
        }
    }
    return (uint64) return_value;
}

static uint64 sem_open(syscall_params params)
{
    uint64 handle = params.p1, init = params.p2;
    int return_value;
    if (!handle)
    {
        return_value = -1;
    }
    else
    {
        *((sem_t *) handle) = new _sem((int) init);
        return_value = 0;
    }
    return (uint64) return_value;
}

static uint64 sem_close(syscall_params params)
{
    uint64 handle = params.p1;
    int return_value;
    if (!handle)
    {
        return_value = -1;
    }
    else
    {
        delete (sem_t) handle;
        return_value = 0;
    }
    return (uint64) return_value;
}

static uint64 sem_wait(syscall_params params)
{
    uint64 handle = params.p1;
    int return_value;
    if (!handle)
    {
        return_value = -1;
    }
    else
    {
        return_value = ((sem_t) handle)->wait();
    }
    return (uint64) return_value;
}

static uint64 sem_signal(syscall_params params)
{
    uint64 handle = params.p1;
    int return_value;
    if (!handle)
    {
        return_value = -1;
    }
    else
    {
        return_value = ((sem_t) handle)->signal();
        ;
    }
    return (uint64) return_value;
}

static uint64 time_sleep(syscall_params params)
{
    uint64 time = params.p1;
    return (uint64) SleepList::sleep(time);
}

static uint64 get_c(syscall_params params)
{
    return (uint64) Console::get_instance().get_c();
}

static uint64 put_c(syscall_params params)
{
    char c = params.p1;
    Console::get_instance().put_c((char) c);
    return 0;
}

static uint64 output_polling(syscall_params params)
{
    Console::get_instance().output_polling();
    return 0;
}

static uint64 output_buffer_size(syscall_params params)
{
    return (uint64) Console::get_instance().output_buffer_size();
}

uint64 (*sys_calls[SYS_CALL_NUMBER])(syscall_params params) = {
        nullptr,
        &mem_alloc,
        &mem_free,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        &thread_create,
        &thread_exit,
        &thread_dispatch,
        &thread_yield,
        &cpp_api_thread_create,
        &cpp_api_thread_start,
        &is_finished_thread,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        &sem_open,
        &sem_close,
        &sem_wait,
        &sem_signal,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        &time_sleep,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        &get_c,
        &put_c,
        &output_polling,
        &output_buffer_size};
