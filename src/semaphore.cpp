#include "../h/semaphore.hpp"
#include "../h/riscv.hpp"

_sem::~_sem()
{
    m_value = 0;
    m_closed = true;

    while (!m_blocked_thread_queue.empty())
    {
        unblock();
    }
}

int _sem::wait()
{
    if (m_closed)
    {
        return -1;
    }

    if (--m_value < 0)
    {
        block();
    }

    return (m_closed ? -1 : 0);
}

int _sem::signal()
{
    if (m_closed)
    {
        return -1;
    }

    if (++m_value <= 0)
    {
        unblock();
    }

    return 0;
}

int _sem::get_value() const { return m_value; }

void _sem::block()
{
    m_blocked_thread_queue.push_back(_thread::running);
    _thread::running->set_blocked(true);
    _thread::dispatch();
}

void _sem::unblock()
{
    if (!m_blocked_thread_queue.empty())
    {
        _thread *t = m_blocked_thread_queue.pop_front();
        t->set_blocked(false);
        Scheduler::put(t);
    }
}
