#include "../h/thread.hpp"
#include "../h/riscv.hpp"

_thread *_thread::running = nullptr;
_thread *_thread::idle_thread = nullptr;
int _thread::_id = 0;
uint64 _thread::time_slice_counter = 0;

_thread *_thread::create_and_start_thread(_thread::Body body, void *arg,
                                          uint64 *stack)
{
    _thread *t = create_thread(body, arg, stack);
    t->start_thread();
    return t;
}

void _thread::start_thread()
{
    if (body != nullptr)
    {
        Scheduler::put(this);
    }
}

_thread *_thread::create_thread(_thread::Body body, void *arg, uint64 *stack)
{
    return new _thread(body, arg, stack);
}

void _thread::dispatch()
{
    time_slice_counter = 0;
    _thread *old = running;

    if (!old->is_finished() && !old->is_blocked() && !old->is_sleeping() &&
        old != idle_thread)
    {
        Scheduler::put(old);
    }

    running = Scheduler::get();

    if (!running)
    {
        if (!idle_thread)
        {
            idle_thread =
                    new _thread([](void *_arg) { thread_dispatch(); }, nullptr,
                                (uint64 *) MemoryAllocator::get_instance().malloc(
                                        DEFAULT_STACK_SIZE * sizeof(uint64)));
        }
        running = idle_thread;
    }

    running->is_user_thread() ? Riscv::mc_sstatus(Riscv::SSTATUS_SPP)
                              : Riscv::ms_sstatus(Riscv::SSTATUS_SPP);

    contextSwitch(&old->context, &running->context);
}

void _thread::thread_wrapper()
{
    Riscv::popSppSpie();
    running->body(running->arg);
    running->set_finished(true);
    thread_dispatch();
}

int _thread::exit()
{
    running->set_finished(true);
    dispatch();
    return 0;
}
