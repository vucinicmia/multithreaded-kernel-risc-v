#ifndef SEMAPHORE_HPP
#define SEMAPHORE_HPP

#include "memory_allocator.hpp"
#include "singly_linked_list.hpp"
#include "syscall_c.h"
#include "thread.hpp"

#include "../lib/hw.h"

/**
 * @brief Semaphore class is a class that is responsible for semaphore
 * operations.
 */
class _sem
{
public:
    explicit _sem(int value = 1) : m_value(value) {}

    ~_sem();

    void *operator new(size_t size)
    {
        return MemoryAllocator::get_instance().malloc(size);
    }

    void operator delete(void *ptr) { MemoryAllocator::get_instance().free(ptr); }

    /**
   * @brief Waits for the semaphore. Decreases the semaphore value by 1. If the
   * semaphore value is less than 0, the thread is blocked.
   *
   * @return 0 if the semaphore was successfully waited, -1 otherwise.
   */
    int wait();

    /**
   * @brief Signals the semaphore. Increases the semaphore value by 1. If there
   * are any threads blocked on the semaphore, one of them is unblocked.
   *
   * @return 0 if the semaphore was successfully signaled, -1 otherwise.
   */
    int signal();

    /**
   * @brief Retrieves the value of the semaphore.
   *
   * @return The value of the semaphore.
   */
    int get_value() const;

private:
    SinglyLinkedList<_thread> m_blocked_thread_queue;

protected:
    int m_value;
    bool m_closed = false;

    void block();
    void unblock();
};

#endif// SEMAPHORE_HPP
