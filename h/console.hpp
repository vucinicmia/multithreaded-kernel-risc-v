#ifndef CONSOLE_HPP
#define CONSOLE_HPP

#include "semaphore.hpp"
#include "synchronized_buffer.hpp"

/**
 * @brief Singleton class that represents the console.
 */
class Console
{
public:
    Console(Console const &) = delete;
    Console(Console &&) = delete;
    Console &operator=(Console const &) = delete;
    Console &operator=(Console &&) = delete;

    /**
   * @brief Returns the instance of the console.
   *
   * @return the instance of the console
   */
    static Console &get_instance();

    /**
   * @brief Inserts a character into the output buffer.
   *
   * @param c The character to insert.
   */
    void put_c(char c);

    /**
   * @brief Removes a character from the input buffer.
   *
   * @return The removed character.
   */
    char get_c();

    /**
   * @brief Handles the interrupts from console and fills the input buffer.
   */
    void interrupt_handler();

    /**
   * @brief Polls the output register and sends characters from the output
   * buffer to the console.
   */
    void output_polling();

    /**
   * @brief Returns the size of the output buffer.
   *
   * @return The size of the output buffer.
   */
    int output_buffer_size();

private:
    Console() = default;

    SynchronizedBuffer<char> m_buffer_in;
    SynchronizedBuffer<char> m_buffer_out;
};

#endif// CONSOLE_HPP
