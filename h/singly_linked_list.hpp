#ifndef SINGLY_LINKED_LIST_HPP
#define SINGLY_LINKED_LIST_HPP

#include "memory_allocator.hpp"

/**
 * @brief SinglyLinkedList class is a class that is responsible for singly
 * linked list operations.
 */
template<typename T>
class SinglyLinkedList
{
private:
    struct Node {
        T *data;
        Node *next;

        Node(T *data, Node *next) : data(data), next(next) {}

        void *operator new(size_t size)
        {
            return MemoryAllocator::get_instance().malloc(size);
        }

        void operator delete(void *ptr)
        {
            MemoryAllocator::get_instance().free(ptr);
        }
    };

public:
    SinglyLinkedList() : m_head(0), m_tail(0), m_num_of_elem(0) {}

    SinglyLinkedList(const SinglyLinkedList<T> &) = delete;

    SinglyLinkedList<T> &operator=(const SinglyLinkedList<T> &) = delete;

    /**
   * @brief Pushes the element to the front of the list.
   *
   * @param data The element to be pushed.
   */
    void push_front(T *data)
    {
        Node *node = new Node(data, m_head);
        m_head = node;
        if (!m_tail)
        {
            m_tail = m_head;
        }
        m_num_of_elem++;
    }

    /**
   * @brief Pushes the element to the back of the list.
   *
   * @param data The element to be pushed.
   */
    void push_back(T *data)
    {
        Node *node = new Node(data, 0);
        if (m_tail)
        {
            m_tail->next = node;
            m_tail = node;
        }
        else
        {
            m_head = m_tail = node;
        }
        m_num_of_elem++;
    }

    /**
   * @brief Pops the element from the front of the list.
   *
   * @return The element that was popped.
   */
    T *pop_front()
    {
        if (!m_head)
        {
            return 0;
        }

        Node *node = m_head;
        m_head = m_head->next;
        if (!m_head)
        {
            m_tail = 0;
        }

        T *data = node->data;

        delete node;

        m_num_of_elem--;

        return data;
    }

    /**
   * @brief Pops the element from the back of the list.
   *
   * @return The element that was popped.
   */
    T *pop_back()
    {
        if (!m_head)
        {
            return 0;
        }

        Node *prev = 0;
        for (Node *curr = m_head; curr && curr != m_tail; curr = curr->next)
        {
            prev = curr;
        }

        Node *elem = m_tail;
        if (prev)
        {
            prev->next = 0;
        }
        else
        {
            m_head = 0;
        }
        m_tail = prev;

        T *ret = elem->data;

        delete elem;

        m_num_of_elem--;

        return ret;
    }

    /**
   * @brief Returns the size of the list.
   *
   * @return The size of the list.
   */
    int size() const { return m_num_of_elem; }

    /**
   * @brief Checks if the list is empty.
   *
   * @return True if the list is empty, false otherwise.
   */
    bool empty() const { return m_num_of_elem == 0; }

    /**
   * @brief Returns the element at the front of the list.
   *
   * @return The element at the front of the list if the list is not empty,
   * nullptr otherwise.
   */
    T *front() const { return m_head ? m_head->data : nullptr; }

    /**
   * @brief Returns the element at the back of the list.
   *
   * @return The element at the back of the list if the list is not empty,
   * nullptr otherwise.
   */
    T *back() const { return m_tail ? m_tail->data : nullptr; }

    ~SinglyLinkedList()
    {
        while (!empty())
        {
            pop_front();
        }
    }

private:
    Node *m_head;
    Node *m_tail;
    int m_num_of_elem;
};

#endif// SINGLY_LINKED_LIST_HPP