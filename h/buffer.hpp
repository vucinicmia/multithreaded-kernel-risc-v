#ifndef BUFFER_HPP
#define BUFFER_HPP

#include "../lib/hw.h"

namespace
{
    static constexpr uint16 BUFFER_SIZE = 256;
}

/**
 * @brief A simple circular buffer implementation.
 *
 * @tparam T The type of the elements in the buffer.
 */
template<typename T>
class Buffer
{
public:
    Buffer() : m_head(0), m_tail(0) {}

    virtual ~Buffer() = default;

    /**
   * @brief Inserts an item into the buffer.
   *
   * @param item The item to insert.
   */
    virtual void insert(const T &item)
    {
        m_buffer[m_tail] = item;
        m_tail = (m_tail + 1) % BUFFER_SIZE;
    }

    /**
   * @brief Removes an item from the buffer.
   *
   * @return The removed item.
   */
    virtual T remove()
    {
        const T item = m_buffer[m_head];
        m_head = (m_head + 1) % BUFFER_SIZE;
        return item;
    }

    /**
   * @brief Returns the item at the head of the buffer.
   *
   * @return The item at the head of the buffer.
   */
    virtual bool empty() { return m_head == m_tail; }

    /**
   * @brief Returns the item at the head of the buffer.
   *
   * @return The item at the head of the buffer.
   */
    virtual bool full() { return (m_head + 1) % BUFFER_SIZE == m_tail; }

    /**
   * @brief Returns the number of items in the buffer.
   *
   * @return The number of items in the buffer.
   */
    virtual int size() { return (m_tail - m_head + BUFFER_SIZE) % BUFFER_SIZE; }

private:
    T m_buffer[BUFFER_SIZE];
    uint16 m_head;
    uint16 m_tail;
};

#endif// BUFFER_HPP