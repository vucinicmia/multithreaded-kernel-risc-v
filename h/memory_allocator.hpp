#ifndef MEMORY_ALLOCATOR_HPP
#define MEMORY_ALLOCATOR_HPP

#include "../lib/hw.h"

/**
 * @brief MemoryAllocator class is a singleton class that is responsible for
 * memory allocation and deallocation.
 */
class MemoryAllocator
{
public:
    MemoryAllocator(MemoryAllocator const &) = delete;
    MemoryAllocator(MemoryAllocator &&) = delete;
    MemoryAllocator &operator=(MemoryAllocator const &) = delete;
    MemoryAllocator &operator=(MemoryAllocator &&) = delete;

    /**
   * @brief Returns the instance of the MemoryAllocator.
   *
   * @return the instance of the MemoryAllocator
   */
    static MemoryAllocator &get_instance();

    /**
   * @brief Allocates memory of the given size.
   *
   * @param size The size of the memory to allocate.
   *
   * @return The address of the allocated memory.
   */
    void *malloc(size_t size);

    /**
   * @brief Frees the memory at the given address.
   *
   * @param addr The address of the memory to free.
   *
   * @return 0 if the memory was successfully freed, -1 otherwise.
   */
    int free(void *addr);

private:
    MemoryAllocator();

    typedef struct mem_node {
        struct mem_node *next;
        struct mem_node *prev;
        uint64 size;
    } MemNode;

    static void insert_node(MemNode **head, MemNode *node);
    static void remove_node(MemNode **head, MemNode *node);
    static void try_to_join(MemNode *node);

    MemNode *m_free_mem_head;
    MemNode *m_alloc_mem_head;
};

#endif// MEMORY_ALLOCATOR_HPP
