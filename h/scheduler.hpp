#ifndef SCHEDULER_HPP
#define SCHEDULER_HPP

#include "singly_linked_list.hpp"
#include "thread.hpp"

class _thread;

/**
 * @brief Scheduler class is a class that is responsible for scheduling the
 * threads.
 */
class Scheduler
{
public:
    /**
   * @brief Returns the thread that is ready to be executed.
   *
   * @return The thread that is ready to be executed.
   */
    static _thread *get();

    /**
   * @brief Puts the given thread to the ready queue.
   *
   * @param tcb The thread to put to the ready queue.
   */
    static void put(_thread *tcb);

private:
    static SinglyLinkedList<_thread> m_ready_thread_queue;
};

#endif// SCHEDULER_HPP