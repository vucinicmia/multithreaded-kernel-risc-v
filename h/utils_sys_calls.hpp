#ifndef SYSCALLS_HPP
#define SYSCALLS_HPP

#define SYS_CALL_NUMBER 100

#include "../lib/hw.h"

typedef enum SYS_CALLS {
    MEM_ALLOC = 0x0000000000000001UL,
    MEM_FREE = 0x0000000000000002UL,
    THREAD_CREATE = 0x00000000000000011UL,
    THREAD_EXIT = 0x00000000000000012UL,
    THREAD_DISPATCH = 0x00000000000000013UL,
    CPP_API_THREAD_CREATE = 0x00000000000000015UL,
    CPP_API_THREAD_START = 0x00000000000000016UL,
    IS_THREAD_FINISHED = 0x00000000000000017UL,
    SEM_OPEN = 0x00000000000000021UL,
    SEM_CLOSE = 0x00000000000000022UL,
    SEM_WAIT = 0x00000000000000023UL,
    SEM_SIGNAL = 0x00000000000000024UL,
    TIME_SLEEP = 0x00000000000000031UL,
    GET_C = 0x00000000000000041UL,
    PUT_C = 0x00000000000000042UL,
    OUTPUT_POLLING = 0x00000000000000043UL,
    OUTPUT_BUFFER_SIZE = 0x00000000000000044UL
} SYS_CALLS;

struct syscall_params {
    uint64 p0;
    uint64 p1;
    uint64 p2;
    uint64 p3;
    uint64 p4;
    uint64 p5;
    uint64 p6;
    uint64 p7;
};

uint64 sys_call_wrapper(syscall_params _params);

/*
 * Used in syscall_c.cpp to place parameters from structure (address of
 * structure is in the a0 register) in registers a0 - a7 before invoking system
 * call.
 */
#define LOAD_SYSCALL_PARAMETERS_IN_REGISTERS()        \
    __asm__ volatile(".irp index, 7,6,5,4,3,2,1,0\n"  \
                     "ld a\\index, \\index * 8(a0)\n" \
                     ".endr")

/*
 * Invokes system call.
 */
#define ECALL __asm__ volatile("ecall")

/*
 * Reads system call return value from register a0 and places it in the variable
 * returnValue.
 */
#define READ_RETURN_VALUE(returnValue) \
    __asm__ volatile("mv %[returnVal], a0" : [returnVal] "=r"(returnValue))

/*
 * Reads system call parameters which were passed in registers a0 - a7.
 * They are read from stack, relative to s0 (frame pointer) since they were
 * saved there before entering trap handler.
 */
#define READ_SYSCALL_PARAMETERS_FROM_REGISTERS(params) \
    __asm__ volatile("mv t0, %[parameter]\n"           \
                     ".irp index, 0,1,2,3,4,5,6,7\n"   \
                     "ld t1, (10 + \\index) * 8(s0)\n" \
                     "sd t1, \\index * 8 (t0)\n"       \
                     ".endr"                           \
                     :                                 \
                     : [parameter] "r"((uint64) & (params)))

/*
 * Saves value in register reg into variable var.
 */
#define SAVE_REGISTER(reg, var) \
    __asm__ volatile("mv %[variable], " reg : [variable] "=r"(var))

/*
 * Restores value of register reg from variable var.
 */
#define RESTORE_REGISTER(reg, var) \
    __asm__ volatile("mv " reg ", %[variable]" : : [variable] "r"(var))

/*
 * Stores return value where register a0 is saved on stack, relative to s0
 * (frame pointer). It will be restored when returned from trap handler.
 */
#define RETURN_VALUE(returnValue)                  \
    __asm__ volatile("sd %[returnVal], 10 * 8(s0)" \
                     :                             \
                     : [returnVal] "r"(returnValue))

#endif// SYSCALLS_HPP
