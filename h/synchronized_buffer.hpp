#ifndef SYNCHRONIZED_BUFFER_HPP
#define SYNCHRONIZED_BUFFER_HPP

#include "buffer.hpp"

#include "../lib/hw.h"

/**
 * @brief A siple synchronized buffer implementation.
 *
 * @tparam T The type of the elements in the buffer.
 */
template<typename T>
class SynchronizedBuffer : public Buffer<T>
{
public:
    SynchronizedBuffer()
        : Buffer<T>(), m_mutex(1), m_space_available(BUFFER_SIZE),
          m_item_available(0) {}

    /**
   * @brief Inserts an item into the buffer.
   *
   * @param item The item to insert.
   */
    void insert(const T &item) override
    {
        m_space_available.wait();
        m_mutex.wait();

        Buffer<T>::insert(item);

        m_mutex.signal();
        m_item_available.signal();
    }

    /**
   * @brief Removes an item from the buffer.
   *
   * @return The removed item.
   */
    T remove() override
    {
        m_item_available.wait();
        m_mutex.wait();

        const T item = Buffer<T>::remove();

        m_mutex.signal();
        m_space_available.signal();
        return item;
    }

    /**
   * @brief Returns the item at the head of the buffer.
   *
   * @return The item at the head of the buffer.
   */
    bool empty() override
    {
        m_mutex.wait();
        const bool result = Buffer<T>::empty();
        m_mutex.signal();
        return result;
    }

    /**
   * @brief Returns the item at the head of the buffer.
   *
   * @return The item at the head of the buffer.
   */
    bool full() override
    {
        m_mutex.wait();
        const bool result = Buffer<T>::full();
        m_mutex.signal();
        return result;
    }

    /**
   * @brief Returns the number of items in the buffer.
   *
   * @return The number of items in the buffer.
   */
    int size() override
    {
        m_mutex.wait();
        const int result = Buffer<T>::size();
        m_mutex.signal();
        return result;
    }

private:
    _sem m_mutex;
    _sem m_space_available;
    _sem m_item_available;
};

#endif// SYNCHRONIZED_BUFFER_HPP