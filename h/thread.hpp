#ifndef THREAD_HPP
#define THREAD_HPP

#include "memory_allocator.hpp"
#include "scheduler.hpp"
#include "syscall_c.h"

#include "../lib/console.h"
#include "../lib/hw.h"

/**
 * @brief _thread class is a class that is responsible for thread operations.
 */
class _thread
{
public:
    ~_thread() { delete[] stack; }

    using Body = void (*)(void *);

    uint64 get_time_slice() const { return time_slice; }

    bool is_user_thread() const { return user_thread; }

    void set_user_thread(bool value) { _thread::user_thread = value; }

    bool is_finished() const { return finished; }

    void set_finished(bool value) { _thread::finished = value; }

    bool is_blocked() const { return blocked; }

    void set_blocked(bool value) { _thread::blocked = value; }

    bool is_sleeping() const { return sleeping; }

    void set_sleeping(bool value) { _thread::sleeping = value; }

    static _thread *create_thread(Body body, void *arg, uint64 *stack);

    static _thread *create_and_start_thread(Body body, void *arg, uint64 *stack);

    void start_thread();

    static void dispatch();

    static _thread *running;

    static _thread *idle_thread;

    static int _id;

    int get_id() const { return id; }

    void *operator new(size_t size)
    {
        return MemoryAllocator::get_instance().malloc(size);
    }

    static int exit();

    static void delete_idle() { delete idle_thread; }

private:
    _thread(Body body, void *arg, uint64 *stack)
        : id(++_id), body(body), arg(arg), stack(stack),
          context({(uint64) &thread_wrapper,
                   stack != nullptr ? (uint64) &stack[DEFAULT_STACK_SIZE] : 0}),
          time_slice(DEFAULT_TIME_SLICE), user_thread(false), finished(false),
          blocked(false), sleeping(false) {}

    struct Context {
        uint64 ra;
        uint64 sp;
    };
    int id;
    Body body;
    void *arg;
    uint64 *stack;
    Context context;
    uint64 time_slice;
    bool user_thread;
    bool finished;
    bool blocked;
    bool sleeping;

    friend class Riscv;

    static void thread_wrapper();

    static uint64 time_slice_counter;

    static void contextSwitch(Context *oldContext, Context *newContext);
};

#endif// THREAD_HPP
