#ifndef SLEEP_LIST_HPP
#define SLEEP_LIST_HPP

#include "memory_allocator.hpp"
#include "thread.hpp"

/**
 * @brief SleepList class is a class that is responsible for sleep list
 * operations.
 */
class SleepList
{
private:
    struct Node {
        _thread *thread;
        int time_sleep;
        Node *next;

        void *operator new(size_t size)
        {
            return MemoryAllocator::get_instance().malloc(size);
        }

        void operator delete(void *ptr)
        {
            MemoryAllocator::get_instance().free(ptr);
        }

        Node(_thread *thread, int time_sleep, Node *next = nullptr)
            : thread(thread), time_sleep(time_sleep), next(next) {}
    };

public:
    /**
   * @brief Inserts the given thread to the sleep list.
   *
   * @param sleeping_thread The thread to insert to the sleep list.
   * @param time_sleep The time to sleep.
   */
    static void insert_element(_thread *sleeping_thread, int time_sleep);

    /**
   * @brief Removes all threads that are ready to be woken up and puts them to
   * the ready queue in the scheduler.
   */
    static void timer_interrupt_handler();

    /**
   * @brief Sleeps for the given time.
   *
   * @param time_sleep The time to sleep.
   *
   * @return 0 if the sleep was successful, -1 otherwise.
   */
    static int sleep(time_t time_sleep);

private:
    static int m_num_of_elem;
    static Node *m_head;
};

#endif// SLEEP_LIST_HPP
