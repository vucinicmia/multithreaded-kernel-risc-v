# Multithreaded kernel for RISC-V architecture

## Project description

The goal of this project was to create some kernel functionalities such as memory management, process scheduling, communication with console etc. This program is designed to run within the educational operating system xv6, which was significantly modified by removing some standard system functionalities. The main role of the host operating system (xv6) is to boot itself, initialize the target hardware, and launch the program. Additionally, the host system provides basic hardware services such as periodic interrupts from the timer and access to the console (keyboard and screen).

In this project I implemented three interfaces for system calls related to memory management, thread management, synchronization primitives, and console communication:
- ABI - the binary interface for system calls made through the software interrupt of the processor. This layer provides the transfer of system call arguments via processor registers, switching to privileged processor mode and kernel code.
- C API -  a procedural programmatic interface for system calls implemented as a set of functions in the C language.
- C++ API - an object-oriented wrapper around functions from the C API layer.

More detailed description is available in [file](docs/Projektni%20zadatak%202022%20v1.1.pdf).